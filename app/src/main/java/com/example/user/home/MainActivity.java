package com.example.user.home;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    LinearLayout layout;
    ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewPager = (ViewPager) findViewById(R.id.viewpager);

        ViewAdapter adapter = new ViewAdapter(getSupportFragmentManager());

        adapter.views.add(new FragmentPageWelcome());
        adapter.views.add(new FragmentPagePemasukan());
        adapter.views.add(new FragmentPagePembayaran());
        adapter.views.add(new FragmentPageRiwayat());
        adapter.views.add(new FragmentPageMutasi());
        adapter.views.add(new FragmentPageLaporan());

        setupIndicators(adapter);

        viewPager.setAdapter(adapter);
    }

    private void setupIndicators(ViewAdapter adapter) {
        layout = (LinearLayout) findViewById(R.id.layout_indicator);

        layout.removeAllViews();

        float density = getResources().getDisplayMetrics().density;
        int dimension = (int) (density * 20);
        int margin = (int) (density*2);

        for (int i = 0;i < adapter.getCount();i++){
            View v = new ImageView(this);
            ViewGroup.MarginLayoutParams params = new ViewGroup.MarginLayoutParams(dimension,dimension);
            params.bottomMargin = margin;
            params.leftMargin = margin;
            params.rightMargin = margin;
            params.topMargin = margin;

            v.setLayoutParams(params);

            layout.addView(v);
        }

        setupIndicatorListeners(adapter);
        setPage(0);

    }

    private void setupIndicatorListeners(final ViewAdapter adapter){
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setPage(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
    void setPage(int page){
        for(int i = 0;i < layout.getChildCount();i++){
            if(i == page){
                ((ImageView)layout.getChildAt(i)).setImageResource(R.drawable.dot_indicator_fill);
            }
            else{
                ((ImageView)layout.getChildAt(i)).setImageResource(R.drawable.dot_indicator_hollow);
            }
        }
    }
    class ViewAdapter extends FragmentPagerAdapter{
        List<Fragment> views = new ArrayList<>();
        public ViewAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return views.get(position);
        }

        @Override
        public int getCount() {
            return views.size();
        }
    }
}
